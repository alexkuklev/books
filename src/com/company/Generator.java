package com.company;

import com.company.model.Book;

public class Generator {

    public static Book[] generate(){

        String[] booksName = {"Война и мир", "Психология", "Анна Каренина", "Автомобили"};
        String[] bookAuthor = {"Лев Толстой", "Рами Блект", "Лев Толстой", "Петров"};
        int[] bookYear = {1867, 1995, 1873, 2000};
        Book[] books = new Book[booksName.length];
        for (int i = 0; i < booksName.length; i++) {
            //Book book = new Book(booksName[i], bookAuthor[i], bookYear[i]);
            books[i] = new Book(booksName[i], bookAuthor[i], bookYear[i]);
        }
        return books;
    }
}
