package com.company.model;

public class Book {
    public String bookName;
    public String bookAuthor;
    public int bookYear;

    public Book(String bookName, String bookAuthor, int bookYear) {
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
        this.bookYear = bookYear;
    }

    public void print(){
        System.out.println(bookName);
        System.out.println(bookAuthor);
        System.out.println(bookYear);
    }

    /*public int getBookYear(){
        return bookYear;
    }*/

    public boolean isOlderThen(Book book){
        return bookYear > book.bookYear;
    }

    public Book compareAndReturnOldest(Book book){
        return bookYear < book.bookYear ? this : book;
    }

    public boolean hasAuthor (String name){
        return bookAuthor.equals(name);
    }

    /*public boolean printIfAuthorEquals(String name){

    }*/

}
