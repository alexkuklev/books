package com.company;

import com.company.model.Book;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int countBookSearchAuthor = 0;
        int countBookSearchYear = 0;

        Book[] books = Generator.generate();
        Library library = new Library(Generator.generate());

        library.printAll();

        System.out.println("Автор самой старой книги: " + library.getOldestBookName());
        //library.getOldestBookName().;

        System.out.println("------------------");
        System.out.println("Введите автора книги:");
        String authorSearch = in.nextLine();
        System.out.println("Книги автора: " + authorSearch);
        for (Book book : books) {
            if (book.hasAuthor(authorSearch)) {
                System.out.println(book.bookName);
                countBookSearchAuthor += 1;
            }
        }
        if (countBookSearchAuthor == 0) {
            System.out.println("Книг введеного вами автора нет.");
        }

        System.out.println("-------------------");
        System.out.println("Введите год книги:");
        int yearBookSearch = in.nextInt();
        for (Book book : books) {
            if (book.bookYear < yearBookSearch) {
                System.out.println("-----");
                System.out.println(book.bookName);
                System.out.println(book.bookAuthor);
                System.out.println(book.bookYear);
                countBookSearchYear += 1;
            }
        }
        if (countBookSearchYear == 0) {
            System.out.println("Книг, написанных ранее " + yearBookSearch + "-го года нет");
        }
    }
}
