package com.company;

import com.company.model.Book;

public class Library {
    Book[] books;

    public Library(Book[] books) {
        this.books = books;
    }

    public void printAll(){
        System.out.println("Список книг:");
        for (Book book : books) {
            System.out.println("---");
            book.print();
        }
    }

    public String getOldestBookName(){
        Book minBookYear = books[0];
        for (int i = 1; i < books.length; i++) {
            /*if (books[i].bookYear < minYearBook) {
                minBookYear = books[i];
            }*/
            /*if(minBookYear.isOlderThen(books[i])){
                minBookYear = books[i];
            }*/
            minBookYear = minBookYear.compareAndReturnOldest(books[i]);
        }
        //System.out.println("Автор самой старой книги: " + minBookYear.bookAuthor);
        return minBookYear.bookAuthor;
    }
}
